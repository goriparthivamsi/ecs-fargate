variable "region" {
  default     = "us-east-1"
  description = "aws region"
}

variable "vpc_cidr" {
  default     = "10.0.0.0/16"
  description = "VPC CIDR Block"
}

variable "public_subnet_1_cidr" {
  description = " public_cidr_1"
}

variable "public_subnet_2_cidr" {
  description = "public_cidr_2"
}

variable "public_subnet_3_cidr" {
  description = "public_cidr_3"
}
variable "private_subnet_1_cidr" {
  description = "private_cidr_1"
}
variable "private_subnet_2_cidr" {
  description = "private_cidr_2"
}
variable "private_subnet_3_cidr" {
  description = "private_cidr_3 "
}
