# remote state
remote_state_key = "prod/infrastructure.tfstate"
remote_state_bucket = "ecs-terraform-state-file"

ecs_domain_name = "goriparthi.org"
ecs_cluster_name = "Production-ECS-Cluster"
internet_cidr_blocks = "0.0.0.0/0"